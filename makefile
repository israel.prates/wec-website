#    This file is part of wec-website.
# 
#    wec-website is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
# 
#    wec-website is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
# 
#    You should have received a copy of the GNU General Public License
#    along with wec-website.  If not, see <https://www.gnu.org/licenses/>.

tgt = index.html
src = index.txt
css_dir = css/

.PHONY: all clean

all: $(tgt)

$(tgt): $(src) $(css_dir)
	rst2html --stylesheet-dirs=$(css_dir) $(src) $(tgt)
	chmod ugo+r $(tgt)

clean:
	rm -rf $(tgt)
