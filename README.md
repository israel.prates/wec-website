## Site do Workshop em Estudos de Computação
---
### Dependências:

#### Docutils
Baixando a última versão:
```
wget https://files.pythonhosted.org/packages/93/22/953e071b589b0b1fee420ab06a0d15e5aa0c7470eb9966d60393ce58ad61/docutils-0.15.2.tar.gz
```

Instalação:

```shell
tar -xvzf docutils-0.15.2.tar.gz
cd docutils-0.15.2
sudo python setup.py install
```

Depois, basta entrar no diretório do _wec-website_ e rodar:

```
make
```

